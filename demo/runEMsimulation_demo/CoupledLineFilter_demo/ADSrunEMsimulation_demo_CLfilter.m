clear all
clc
close all

addpath(genpath('\\elec-stor1-w2k8\vagevuur\toolbox'))

% CLfilter_demo.net contains is an ADS netlist that contains several
% instances of transmission lines. The ADScreateLayout function is called
% to create the layout file. The warning about the Port component from
% ASDScreateLayout can be ignored 

% generate the default options struct for momentum simulations
options = ADSgenerateOptFile();
% change the number of cells per wavelength
options.no_cells_per_wavelength = 10;

% the substrate is contained in the proj.ltd file

% run the simulation, with the following bonus options
%   show the generated lay-out with the mesh
%   override the frequency grid with 'fstart', 'fstop' and 'dec'
%   use the custom momentum options specified in the options struct
res  = ADSrunEMsimulation('CLfilter_demo.net','showGeneratedLayout',true,...
    'fStart',4e9,'fStop',7e9,'fStep',10e6,'optSettings',options);


%% plot the simulation result
figure
leg=plot_mimo(res.freq,db(res.S));
legend(leg)
