% test code
% date: 26/05/2014
% matthias ELEC

clear variables
clc
close all

cd(fileparts(mfilename('fullpath')));

wfeed=1;
lfeed=8;

w = 1;
a = 16.35;
t_in = 2.16;
t_out = 2.16;

d12 = 1.39;
d14 = 2.56;
d23 = 2.15;
d34 = 1.99;
d45 = 2.6;
d56 = 2.28;
d58 = 1.65;
d67 = 1.8;
d78 = 1.52;

%Parameters resonators

g1 = 1.9;
tx1 = 0;
ty1 = -(a/2-t_in);

g2 = 1.61;
tx2 = a+abs(d14-d23)/2;
ty2 = -(a+d12);

g3 = 1.67;
tx3 = a+d23+abs(d14-d23)/2;
ty3 = -(d34+a);

g4 = 1.7;
tx4 = 2*a+d14;
ty4 = 0;

g5 = 1.72;
tx5 = d45;
ty5 = 0;

g6 = 1.64;
tx6 = a-abs(d67-d58)/2;
ty6 = d56+a;

g7 = 1.65;
tx7 = a+d67-abs(d67-d58)/2;
ty7 = d78+a;

g8 = 1.85;
tx8 = 2*a+d58;
ty8 = 0;


% create the netlist 
netlist={};
netlist{end+1} = sprintf('MLIN:feedLine Nf0 Nf1 W=%s mm L=%s mm',...
    num2str(wfeed),num2str(lfeed));
netlist{end+1} = sprintf('SOLR:res1 Nf1 N1 W=%s mm A=%s mm G=%s mm OffX=%s mm OffY=%s mm  orientation=0',...
    num2str(w),num2str(a),num2str(g1),num2str(tx1),num2str(ty1));
netlist{end+1} = sprintf('SOLR:res2 N1 N2 W=%s mm A=%s mm G=%s mm OffX=%s mm OffY=%s mm  orientation=180',...
    num2str(w),num2str(a),num2str(g2),num2str(tx2),num2str(ty2));
netlist{end+1} = sprintf('SOLR:res3 N1 N3 W=%s mm A=%s mm G=%s mm OffX=%s mm OffY=%s mm  orientation=0',...
    num2str(w),num2str(a),num2str(g3),num2str(tx3),num2str(ty3));
netlist{end+1} = sprintf('SOLR:res4 N1 N4 W=%s mm A=%s mm G=%s mm OffX=%s mm OffY=%s mm  orientation=180',...
    num2str(w),num2str(a),num2str(g4),num2str(tx4),num2str(ty4));
netlist{end+1} = sprintf('SOLR:res5 N4 N5 W=%s mm A=%s mm G=%s mm OffX=%s mm OffY=%s mm  orientation=0',...
    num2str(w),num2str(a),num2str(g5),num2str(tx5),num2str(ty5));
netlist{end+1} = sprintf('SOLR:res6 N5 N6 W=%s mm A=%s mm G=%s mm OffX=%s mm OffY=%s mm  orientation=180',...
    num2str(w),num2str(a),num2str(g6),num2str(tx6),num2str(ty6));
netlist{end+1} = sprintf('SOLR:res7 N5 N7 W=%s mm A=%s mm G=%s mm OffX=%s mm OffY=%s mm  orientation=0',...
    num2str(w),num2str(a),num2str(g7),num2str(tx7),num2str(ty7));
netlist{end+1} = sprintf('SOLR:res6 N5 N8 W=%s mm A=%s mm G=%s mm OffX=%s mm OffY=%s mm orientation=180',...
    num2str(w),num2str(a),num2str(g8),num2str(tx8),num2str(ty8));
netlist{end+1} = sprintf('MLIN:line N8 Nf3 W=%s mm L=%s mm orientation=-90',...
    num2str(0),num2str((a/2-t_out)));
netlist{end+1} = sprintf('MLIN:FeedLine2 Nf3 Nf4 W=%s mm L=%s mm orientation=90',...
    num2str(wfeed),num2str(lfeed));
netlist{end+1} = 'Port:P1 Nf0 0 Num=1 Z0=50 Ohm';
netlist{end+1} = 'Port:P2 Nf4 0 Num=2 Z0=50 Ohm';

%frequency in GHz
fstart = 0.850e9;
fstep  = 0.005e9;
fstop  = 1.150e9;

% write the netlist to a temporary file
writeTextFile(netlist,'temp');
% call the ADSrunEMsimulation function to do all the heavy lifting
res= ADSrunEMsimulation('temp','fstart',fstart,'fstop',fstop,'fstep',fstep,'showGeneratedLayout',true,'cleanup',false);
% delete the temporary file
delete('temp');

if ~isempty(res)
figure;
plot(res.freq, squeeze(db(res.S(2,1,:))),'-bo')
% xlabel('Frequency (GHz)')
% ylabel('|S_{21}| (dB)')

figure,plot(res.freq,squeeze(db(res.S(1,1,:))),'-bo')
% xlabel('Frequency (GHz)')
% ylabel('|S_{11}| (dB)')
end