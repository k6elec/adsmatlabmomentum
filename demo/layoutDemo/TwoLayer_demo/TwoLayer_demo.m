% This script simulates a simple two-layer structure in Momentum. 
%
% The netlist looks as follows:
%   Port:P1 N1 N3 Num=1 Z0=50 Ohm Node2Layer=2
%   MLIN:TL1 N1 N2 W=3.4 mm L=42 mm
%   MLIN:TL2 N3 N8 W=10 mm L=42 mm Layer=2
%   Port:P2 N2 N4 Num=2 Z0=50 Ohm Node2Layer=2
%
% The ports are connected between the two layers. TL1 is the transmission
% line on the top, while TL2 acts as the finite ground plane
%
% The substrate is defined in "Ro4003_twolayer.ltd", I generated this file in ADS
clear variables
close all
clc


ADSrunEMsimulation('TwoLayer.net','substrateFile','Ro4003_twolayer.ltd',...
    'fstart',1e9,'fstop',1e9,'fstep',1e9,...
    'showGeneratedLayout',true,'cleanup',true);

