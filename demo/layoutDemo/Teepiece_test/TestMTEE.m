clear variables
close all
clc

cd(fileparts(mfilename('fullpath')))

w=1;
wfeed = 1;
lfeed = 2;

netlist={};
netlist{end+1} = sprintf('MLIN:feedLine Nf0 Nf1 W=%s mm L=%s mm',num2str(wfeed),num2str(lfeed));
netlist{end+1} = sprintf('MTEEa:tee N1 N2 Nf1 W1=%s mm W2=%s mm W3=%s mm orientation=270 Up=0',num2str(w),num2str(w),num2str(w));
netlist{end+1} = 'Port:P1 Nf0 0 Num=1 Z0=50 Ohm';

netliststruct = ADSparseNetlist(netlist);

res = ADSrunEMsimulation(netliststruct,'fstart',1e9,'fstop',2e9,'fstep',0.5e9,'showGeneratedLayout',true);