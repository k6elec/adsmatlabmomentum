function engstr = eng(value, nDigits, nDecimals)
% ENG returns the engineering notation of a number
% 
%  Usage:
%       eng(value)
%       eng(value, nDigits)
%       eng(value, [], nDecimals)
%  
%  With:
%    value: the value to be converted
%    nDigits: (optional) total number of digits, default = 5
%    nDecimals: (optional) number of decimals
%
%  See also: sprintf, num2str

%% Constants
IMAG_SYMBOL = 'j';
PREFIXPOWER = -18:3:12;
PREFIXNAME  = {'a','f','p','n','u','m',' ','k','M','G','T'};

%% Strings pass through
if ischar(value)
    engstr = value;
    return;
end

%% Default Values
formatType = 'g';
if ~exist('nDigits','var')
    nDigits = 6;
end;
if exist('nDecimals','var')
    formatType = 'f';
    nDigits = nDecimals;
end;

%% Handle Complex Values
if ~isreal(value)
    switch formatType
        case 'g'
            rePart = eng(real(value),nDigits);
            imPart = eng(imag(value),nDigits);
        case 'f'
            rePart = eng(real(value),[],nDecimals);
            imPart = eng(imag(value),[],nDecimals);
        otherwise
            rePart = eng(real(value));
            imPart = eng(imag(value));
    end
        
    if imPart(1)=='-'
        sign = '-';
        imPart = imPart(2:end);
    else
        sign = '+';
    end
    engstr = sprintf('%s %s %s*%s',rePart,sign,IMAG_SYMBOL,imPart);
    return;
end


%% Choose SI prefix
logValue = log10(abs(value));

if ((PREFIXPOWER(1)-3 < logValue) && (logValue < PREFIXPOWER(end) + 3))
    indPrefix = find((logValue < PREFIXPOWER),1,'first');
    if indPrefix == 1
        prefix =  PREFIXNAME{1};
        power  = PREFIXPOWER(1);
    elseif ~isempty(indPrefix)
        prefix =  PREFIXNAME{indPrefix-1};
        power  = PREFIXPOWER(indPrefix-1);
    else
        prefix =  PREFIXNAME{end};
        power  = PREFIXPOWER(end);
    end
    formatStr = sprintf('%%0.%i%s',nDigits,formatType);
else
    % outside of range
    prefix = '';
    power = 0;
    if formatType(1) == 'f'
        formatStr = sprintf('%%0.%i%s',nDigits-4,'g');
    else
        formatStr = sprintf('%%0.%i%s',nDigits-1,'g');
    end;
end;

engstr = sprintf('%s%s',sprintf(formatStr,value/10^power),prefix);