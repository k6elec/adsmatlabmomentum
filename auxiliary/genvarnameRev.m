function res = genvarnameRev(input)
% GENVARNAMEREV escapes all special characters in a string
% 
%   res = genvarnameRev(input)
%
% where input is a string or cell array of strings
% res a string or cell array of strings.
%
% the function looks for all characters which are not normal letters or
% numbers and replaces them by the letter '_' followed by 3 numbers which
% represent the ascii number of the character that stood there originally.
% To ensure reversible conversion, the character '_' is also considered
% special.
%
% Example:
%   genvarnameRev('hello&goodbye')
%   ans =
%   hello_038goodbye
%   
%   See also: GENVARNAMEINV GENVARNAME
%
% Adam Cooman, ELEC VUB


if iscell(input)
    [m,n] = size(input);
    % todo: check whether all elements in the cell array are strings
    singlestring = false;
elseif ischar(input)
    singlestring = true;
    input = {input};
    [m,n]=size(input);
else
    error('function can only accept strings or cell arrays of strings');
end
    

res = cell(m,n);
for ii=1:m
    for jj=1:n
        res{ii,jj} = escapeString(input{ii,jj});
    end
end

if singlestring
    res = res{1,1};
end

end


function res = escapeString(string)
% actual function that does the job:
% find all weird characters
% replace by _000 with the numbers equal to the ascii number of the
% character that stood there
W = regexp(string,'[^a-z0-9A-Z]');
if ~isempty(W)
    C = regexp(string,'[^a-z0-9A-Z]','split');
    T = cell(1,2*length(C)-1);
    T(1:2:end)=C;
    for ii=1:length(W)
        conv = num2str(uint8(string(W(ii))),'%03i');
        if length(conv)==3
            T{2*ii} = ['_' conv];
        else
            warning('very weird character encountered, watch out! result is not reversible');
        end
    end
    res = [T{:}];
else
    res=string;
end
end