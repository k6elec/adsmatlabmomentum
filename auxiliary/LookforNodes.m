function [currentNode,nodeIndex] = LookforNodes(ComponentNodes,nodelist)
% this function looks for the nodes of a component in the list of nodes in the circuit. 
%
% [currentNode,nodeIndex] = LookforNodes(ComponentNodes,nodelist)
%
% ComponentNodes    is a cell array of node names to which the component under study is connected
% nodelist          is a structure vector that contains all nodes which have been
%                   created in the current design. The nodelist struct contains the following fields
%       nodelist.Name
%       nodelist.x
%       nodelist.y
%       nodelist.orientation
% 
% The function returns the structure from the nodelist that corresponds to the found node 
% together with the index of the node that has been found on the component
%
%   If none of the nodes is found, the "node not found" error is thrown.
%   
%
% Adam Cooman, ELEC VUB


nodeNotFound=1;

% get the node names from the nodelist
nodelistNames = {nodelist.Name};

for nn=1:length(ComponentNodes)
    %find the index of the node in the nodes list that corresponds to a node the MLIN2.
    ind=find(strcmp(ComponentNodes{nn},nodelistNames));
    % check the length of the found indices
    switch length(ind)
        case 0
            % nothing is found, just continue to the following node
        case 1
            % if only one node is found, return that node as the result
            currentNode = nodelist(ind);
            nodeIndex = nn;
            nodeNotFound = false;
            break
        otherwise
            % if multiple nodes are found, we can be in trouble
            error('multiple nodes found');
    end
end

% if we passed all the nodes without finding one in the list, throw the "node not found" error
if nodeNotFound
    err = MException('MOM:NNF', 'node not found');
    throw(err);
end

end