function ADSgenerateSVG(varargin)
% ADSGENERATESVG generates an svg image of the layout in an ads file.
%
%   generateSVG(svg_file,a_file,pin_file)
%
% where svg_file is the filename of the file to write to (default='layout.svg')
% a_file is the file describing the lay-out for the momentum simulation (default='proj_a')
% pin_file is the file describing the location of the pins in the lay-out (default='proj.pin')
%
% Adam Cooman, ELEC VUB

p=inputParser();
p.addOptional('svg_file','layout.svg',@ischar);
p.addOptional('a_file','proj_a',@ischar);
p.addOptional('pin_file','proj.pin',@ischar);
p.parse(varargin{:})
args = p.Results;


%% parse the a_file and save the results in a struct.

a_cell = readTextFile(args.a_file);
layoutstruct = struct;

maxX = -Inf;
minX = Inf;
minY = Inf;
maxY = -Inf;

for ii=3:length(a_cell)-1
    t=regexp(a_cell{ii},'^ADD\s+(?<layer>[a-zA-Z0-9]+)\s+:[a-zA-Z0-9\.]+\s(?<points>[\-e0-9\.,\s]+);$','names');
    if ~isempty(t)
        split = regexp(t.points,'\s','split');
        ind=1;
        clear points
        for jj=1:length(split)
            if ~isempty(split{jj})
                split2=regexp(split{jj},',','split');
                Xpoint=str2double(split2{1});
                Ypoint=-str2double(split2{2});
                points(ind,:) = [Xpoint Ypoint];
                % check the maximum
                if Xpoint>maxX
                    maxX=Xpoint;
                end
                if Xpoint<minX
                    minX=Xpoint;
                end
                if Ypoint>maxY
                    maxY=Ypoint;
                end
                if Ypoint<minY
                    minY=Ypoint;
                end
                ind=ind+1;
            end
        end
        layoutstruct.(['shape' num2str(ii)]).points = points;
        layoutstruct.(['shape' num2str(ii)]).layer = t.layer;
    else
        error('something went wrong during parsing');
    end
end


%% parse the pin file to find the location of the ports

% remove the comment from the pin_file
pin_cell = readTextFile(args.pin_file);
fid = fopen('temp','w');
for ii=1:length(pin_cell)
    if isempty(regexp(pin_cell{ii},'^\s*<!--','once'))
        fprintf(fid,'%s\r\n',pin_cell{ii});
    end
end
fclose(fid);
pin_struct = xml2struct('temp');
delete('temp');

for ii=1:length(pin_struct.pin_list.pin)
    layoutstruct.(['PIN' num2str(ii)]).layer = ['P' pin_struct.pin_list.pin{1,ii}.layout.shape.layer.Text];
    Xpoint = str2double(pin_struct.pin_list.pin{1,ii}.layout.shape.point.x.Text);
    Ypoint = -str2double(pin_struct.pin_list.pin{1,ii}.layout.shape.point.y.Text);
    % The pin coordinates are given in meters, the others are given in mm.
    layoutstruct.(['PIN' num2str(ii)]).points = [Xpoint*1000 Ypoint*1000];
end


%% shift all the points such that all coordinates are positive
fields=fieldnames(layoutstruct);
for ii=1:length(fields)
    layoutstruct.(fields{ii}).points(:,1)=layoutstruct.(fields{ii}).points(:,1)-minX;
    layoutstruct.(fields{ii}).points(:,2)=layoutstruct.(fields{ii}).points(:,2)-minY;
end

figWidth=maxX-minX;
figHeight=maxY-minY;

%% now generate the SVG file

fid = fopen(args.svg_file,'w');
layercolors = {'red','blue','yellow'};

% put the xml definition
fprintf(fid,'<?xml version="1.0" encoding="UTF-8" standalone="no"?>\r\n');
% start the svg definition
fprintf(fid,'<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" width="%smm" height="%smm" viewBox="0 0 %s %s" version="1.1" id="svg2">\r\n',num2str(figWidth),num2str(figHeight),num2str(figWidth),num2str(figHeight));
% put some info for the view in inkscape
% fprintf(fid,'<sodipodi:namedview pagecolor="#ffffff" bordercolor="#666666" inkscape:document-units="px" inkscape:window-x="%s" inkscape:window-y="%s" inkscape:window-height="%s" inkscape:window-width="%s"></sodipodi:namedview>\r\n',num2str(),num2str(),num2str(maxX-minX),num2str(maxY-minY));
% metadata can be placed here, I don't think I need that.

layernumber=1;
while ~isempty(fieldnames(layoutstruct))
    fields = fieldnames(layoutstruct);
    layer = layoutstruct.(fields{1}).layer;
    fprintf(fid,'<g id="%s">\r\n',layer);
    fprintf(fid,[drawSVGpolyline(layoutstruct.(fields{1}).points,layercolors{layernumber}) '\r\n']);
    for ii=2:length(fields)
        if strcmp(layoutstruct.(fields{ii}).layer,layer)
            if size(layoutstruct.(fields{ii}).points,1)==1
                fprintf(fid,[drawSVGport(layoutstruct.(fields{ii}).points,layercolors{layernumber}) '\r\n']);
            else
                fprintf(fid,[drawSVGpolyline(layoutstruct.(fields{ii}).points,layercolors{layernumber}) '\r\n']);
            end
            layoutstruct=rmfield(layoutstruct,fields{ii});
        end
    end
    layoutstruct=rmfield(layoutstruct,fields{1});
    fprintf(fid,'</g>\r\n');
    layernumber=layernumber+1;
end
fprintf(fid,'</svg>');

fclose(fid);

end






function res = drawSVGpolyline(points,color)
    res=sprintf('<path style="fill:%s;fill-opacity:0.5;stroke:#000000;stroke-width:0.01px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"',color);
    res=[res ' d="'];
    for ii=1:length(points)
        if ii==1
            res=[res ' M ' num2str(points(ii,1)) ',' num2str(points(ii,2))];
        else
            res=[res ' L ' num2str(points(ii,1)) ',' num2str(points(ii,2))];
        end
    end
    % z indicates a closed path
    res = [res,'z" />'];
end

function res = drawSVGport(points,color)
    res = sprintf('<circle cx="%s" cy="%s" r="0.1px" stroke="black" stroke-width="0.01px" fill="%s" />',num2str(points(1,1)),num2str(points(1,2)),color);
end



%% Syntax
% generateSVG(svg_file,a_file,pin_file)
% 
%% Inputs
% svg_file | filename of the svg file created by the function
% a_file | filename of the _a file that has to be processed by the function
% pin_file | .pin file associated with the _a file
%
%% Outputs
%  | The function generates an svg file which contains a drawing of the
%  | layout in the _a and the .pin file
%
%
%% Version History
% Adam |    12/2013 | version 1.0
% 
% copyright: ELEC, VUB