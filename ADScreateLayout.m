function [nodes,polygons] = ADScreateLayout(layout,startNode)
% ADSCREATELAYOUT creates an array of nodes and polygons from a layoutstruct which contains structures that represent the elements
%
%   [nodes,polygons] = ADScreateLayout(layout,startNode)
%
% Where layout is a layout struct which is generated by parsing a netlist
%   with the ADSparseNetlist function.
% startNode is a struct which contains the first node to be used to
%   generate the lay-out.
%
% nodes is the list with nodes that has not been used yet
% polygons is the list of polygons which represents the lay-out.
%
% Matthias Caenepeel and Adam Cooman, ELEC VUB
% 17/02/2014 Version 1
% 09/03/2014 Added comments and changed the working of the function slightly
% 12/01/2015 Fixed a bug where the polygons struct was not initialised and added code to ignore comments in the netlist
% 12/01/2015 Added the starting node back to the nodelist at the end. This is to make sure that ports can be connected to it lateron

notFinished=1;
% nc is the number of placed components
nc=0;

% the starting node is the first node in the nodes list
nodes=startNode;

% initialise the polygons struct
polygons=struct('Coord',{},'Layer',{});

% remove all comments from the netlist
if isfield(layout,'StatementType');
    layout = layout(cellfun(@isempty,regexpi({layout.StatementType},'^COMMENT$','once')));
end

% generate the layout by going through all components and generating their layout
while notFinished
    % save the old amount of placed components
    nc_old=nc;
    
    % run over the layoutstruct and try to playce the components
    for n=1:length(layout)
        % create the function handle to the appropriate function
        funStr=['create' layout(1,n).Type];
        fhandle=str2func(funStr);
        try
            % try calling the function
            [nodes,polygons]=feval(fhandle,layout(1,n),polygons,nodes);
            % if succeeded, increase the number of placed components
            nc=nc+1;
        catch ME
            switch ME.identifier
                case 'MATLAB:UndefinedFunction'
                    % if the function is not found, give a warning and ignore the component
                    warning('ADSLayout:UnknownType','Cannot handle "%s" of field %s with message: %s',layout(1,n).Type,num2str(n),ME.message);
                    nc=nc+1;
                case {'NNF','MOM:NNF'} %(NNF= nodes not found)
                    % if the function returns a NNF error, skip it for now
                otherwise
                    % all other errors are serious and interrupt the lay-outing
                    rethrow(ME);
            end
        end
    end
    % if all the components are placed, the lay-outing is finished
    if(nc==length(layout))
        notFinished=0;
    end
    % if no new components are placed during a step, the lay-outing is stuck
    if(nc_old==nc)
        error('lay-outing is stuck, several components could not be placed.\r\n Make sure that all elements are connected together');
    end
end

% add the startNode back to the nodes list, such that ports can be added to it lateron
nodes(end+1)=startNode;

end

