function [nodes,polygons] = createPort(structure,polygons,nodes,orientation)
% createPort.m adds the nodes to the array of nodes and also creates the polygon
% that corresponds to the MLIN2 structures and adds it to the polygon list.
% Needed fields are:
%   structure   This structure contains the information about the component
%               that is created. In this case a single microstrip line. It has
%               following fields:
%               structure.parameters.Name: Name of the component.
%               structure.parameters.L: Length of the line pair.
%               structure.parameters.W: Width of the line pair.
%               structure.parameters.Nodes: contains the nodes of the
%               components
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
% The struct arrays nodes and polygons are updated.
% Matthias Caenepeel, ELEC
% 18/02/2014 V1



% look for one of the nodes in the nodelist
nodeNotFound=1;
i1=1;
while (nodeNotFound)
    % find the index of the node in the nodes list that corresponds to a node
    j1=find(cellfun(@any,regexp({nodes.Name},['^' structure.Nodes{i1} '$'])));
    % choose the node corresponding to that index as the current node
    currentNode=nodes(j1);
    switch(length(currentNode))
        case(1)
            % if a node is found quit the while loop
            nodeNotFound=0;
            % find the index of the node of the MLIN
            nodeIndex=i1;
        case(0)
            if(i1==length(structure.Nodes))
                % no corresponding node in the nodes list
                % error('MOM:NNF','node not found');
                err = MException('MOM:NNF', 'node not found');
                throw(err);
            end
        otherwise
            error('multiple nodes found')
    end
    i1=i1+1;
end

% we now have one of the nodes of the component. the nodeIndex indicates
% which one has been found.
% Now create the needed polygons and add extra nodes to the list of nodes

switch nodeIndex
    case 1
        % if the second node is ground, then I don't have to add more nodes
        if ~strcmp(structure.Nodes{2},'0')
            % if it is not ground, I just add the second node to the
            % nodelist a the same location as the first node and with the
            % same orientation
            newNode.Name=structure.Nodes{2};
            newNode.x=currentNode.x;
            newNode.y=currentNode.y;
            newNode.Orientation=currentNode.Orientation;
        end
    case 2
        % in case the second node is found first, just add the first node
        % to the nodelist with the same orientation
        if ~strcmp(structure.Nodes{1},'0')
            %update the node, add other node to nodes list
            newNode.Name=structure.Nodes{1};
            newNode.x=currentNode.x;
            newNode.y=currentNode.y;
            newNode.Orientation=currentNode.Orientation;
        else
            error('It is not allowed to have a port connected to the minus pin only\n "Port:%s 0 %s ..." is not allowed',structure.Name,structure.Nodes{2});
        end
    otherwise
        error('I magically found a %dth node in the Port, weird',nodeIndex);
end



% add the new node to the list, leave the original node in the list
nodes(end+1)=newNode;



end

