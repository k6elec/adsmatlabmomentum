function [nodes polygons] = createMLIN2(structure,polygons,nodes,orientation)
%createMLIN2.m adds the nodes to the array of nodes and also creates the polygon
%that corresponds to the MLIN2 structures and adds it to the polygon list.
%Needed fields are:
%   structure   This structure contains the information about the component
%               that is created. In this case a single microstrip line. It has
%               following fields:
%               structure.parameters.Name: Name of the component.
%               structure.parameters.L: Length of the line pair.
%               structure.parameters.W: Width of the line pair.
%               structure.parameters.Nodes: contains the nodes of the
%               components
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
% The struct arrays nodes and polygons are updated.
%Matthias Caenepeel, ELEC
%18/02/2014 V1

%PATTERN = '\d*\.?\d+';
%This pattern is used to find the value and the unit of the parameters
%using the function regexp: ?<value> will fill in the field value with zero
%or more digits (*) a possible point \.? and one or more d, a space and
%than it fills in the field unit with zero or more word characters \w*
PATTERN2 = '(?<value>\d*\.?\d+)\s*(?<unit>\w*)';  

%Find the parameters from the structure MLIN2
W=regexp(structure.Parameters.W,PATTERN2,'names');
w=str2double(W.value);

switch W.unit
    case 'mm'
        w=w*1e-3;
    case ''
    otherwise
        error('Unit not known')
end

L=regexp(structure.Parameters.L,PATTERN2,'names');
l=str2double(L.value);

switch L.unit
    case 'mm'
        l=l*1e-3;
    case ''
    otherwise
        error('Unit not known')
end

%Check if there is node in the nodes list that corresponds to a node of the MLIN2. If
%there is no node, there is something wrong!

nodeNotFound=1;
i1=1;

while (nodeNotFound)
 
 %find the index of the node in the nodes list that corresponds to a node
 %the MLIN2.
 j1=find(cellfun(@any,regexp({nodes.Name},['^' structure.Nodes{i1} '$'])));
 %choose the node corresponding to that index as the current node
 currentNode=nodes(j1);
 switch(length(currentNode))
     case(1)
         %if a node is found quit the while loop
         nodeNotFound=0;
         %find the index of the node of the MLIN
         nodeIndex=i1;
     case(0)
         if(i1==length(structure.Nodes))
           %no corresponding node in the nodes list
           error('NNF')  
         end
     otherwise 
         error('multiple nodes found')
 end
 i1=i1+1;
end

%initialize the polygon coordinates (it is known that it will be a
%rectangle): thus 4 points with 2 coordinates (a,1)= x coordinate of point
%a, (a,2) = y coordinate of point a
poly=zeros(4,2);


if exist('orientation','var')
    currentNode.Orientation=currentNode.Orientation+orientation;
end

theta=currentNode.Orientation*(pi/180);


switch(nodeIndex)
    case(1)
       %choose the reference x,y coordinates
       x=currentNode.x;
       y=currentNode.y;
      
       %update the node, add other node to nodes list
       newNode.Name=structure.Nodes{2};
       newNode.x=x+l*cos(theta);
       newNode.y=y+l*sin(theta);
       newNode.Orientation=currentNode.Orientation;
       
     case(2)
       %choose the reference x,y coordinates
       x=currentNode.x-cos(theta)*l;
       y=currentNode.y-sin(theta)*l;
       
       %update the node, add other node to nodes list
       newNode.Name=structure.Nodes{1};
       newNode.x=x;
       newNode.y=y;
       newNode.Orientation=currentNode.Orientation;
end

%calculate coordinates of polygon
poly(1,1)=x-sin(theta)*(w/2);
poly(1,2)=y+cos(theta)*(w/2);

poly(2,1)=x+l*cos(theta)-sin(theta)*(w/2);
poly(2,2)=y+l*sin(theta)+cos(theta)*(w/2);

poly(3,1)=x+l*cos(theta)+sin(theta)*(w/2);
poly(3,2)=y+l*sin(theta)-cos(theta)*(w/2);

poly(4,1)=x+sin(theta)*(w/2);
poly(4,2)=y-cos(theta)*(w/2);

%update polygons array
polygons(end+1)=struct('Coord',poly,'Layer',1);
%update nodes
nodes(j1)=newNode;


end

