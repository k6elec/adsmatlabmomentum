function [nodes polygons] = createMCLIN(structure,polygons,nodes,orientation)
%createMCLIN.m adds the nodes to the array of nodes and also creates the polygon
%that corresponds to the MCLIN (coupled line pair) structure and adds it to the polygon list.
%Needed fields are:
%   structure   This structure contains the information about the component
%               that is created. In this case a coupled line pair. It has
%               following fields:
%               structure.parameters.Name: Name of the component.
%               structure.parameters.L: Length of the line pair.
%               structure.parameters.W: Width of the line pair.
%               structure.parameters.S: Spacings between the line.
%               structure.parameters.Nodes: contains the nodes of the
%               components
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
% The struct arrays nodes and polygons are updated.
%Matthias Caenepeel, ELEC
%18/02/2014 V1

%PATTERN = '\d*\.?\d+';
%This pattern is used to find the value and the unit of the parameters
%using the function regexp: ?<value> will fill in the field value with zero
%or more digits (*) a possible point \.? and one or more d, a space and
%than it fills in the field unit with zero or more word characters \w*

PATTERN2 = '(?<value>\d*\.?\d+)\s*(?<unit>\w*)';  

%Find the parameters from the structure MLIN2
W=regexp(structure.Parameters.W,PATTERN2,'names');
w=str2double(W.value);

switch W.unit
    case 'mm'
        w=w*1e-3;
    case ''
    otherwise
        error('Unit W not known')
end

L=regexp(structure.Parameters.L,PATTERN2,'names');
l=str2double(L.value);

switch L.unit
    case 'mm'
        l=l*1e-3;
    case ''
    otherwise
        error('Unit L not known')
end

S=regexp(structure.Parameters.S,PATTERN2,'names');
s=str2double(S.value);

switch S.unit
    case 'mm'
        s=s*1e-3;
    case ''
    otherwise
        error('Unit S not known')
end


%Check if there is node in the nodes list that corresponds to a node of the CLIN1. If
%there is no node, there is something wrong!

nodeNotFound=1;
i1=1;

while (nodeNotFound)
 
 %find the index of the node in the nodes list that corresponds to a node
 %the MLIN2.
 j1=find(cellfun(@any,regexp({nodes.Name},['^' structure.Nodes{i1} '$'])));
 %choose the node corresponding to that index as the current node
 currentNode=nodes(j1);
 switch(length(currentNode))
     case(1)
         %if a node is found quit the while loop
         nodeNotFound=0;
         %find the index of the node of the MLIN
         nodeIndex=i1;
     case(0)
         if(i1==length(structure.Nodes))
           %no corresponding node in the nodes list
           errorStruct.message = 'Node not found';
           errorStruct.identifier = 'MOM:NNF';
           error(errorStruct)  
         end
     otherwise 
         error('multiple nodes found')
 end
 i1=i1+1;
end

if exist('orientation','var')
    currentNode.Orientation=currentNode.Orientation+orientation;
end

theta=currentNode.Orientation*(pi/180);

switch(nodeIndex)
    case(1)
       %choose the reference x,y coordinates
       x=currentNode.x;
       y=currentNode.y;
       
       %update the node, add other nodes to nodes list
       newNode.Name=structure.Nodes{2};
       newNode.x=x+(w+s)*sin(theta);
       newNode.y=y-(w+s)*cos(theta);
       newNode.Orientation=currentNode.Orientation;
       
       newNode1.Name=structure.Nodes{4};
       newNode1.x=x+l*cos(theta);
       newNode1.y=y+l*sin(theta);
       newNode1.Orientation=currentNode.Orientation;
       
       newNode2.Name=structure.Nodes{3};
       newNode2.x=x+l*cos(theta)+(w+s)*sin(theta);
       newNode2.y=y-(w+s)*cos(theta)+l*sin(theta);
       newNode2.Orientation=currentNode.Orientation;
       
      case(2)
       %choose the reference x,y coordinates
       x=currentNode.x-(w+s)*sin(theta);
       y=currentNode.y+(w+s)*cos(theta);
       
       %update the node, add other nodes to nodes list
       newNode.Name=structure.Nodes{1};
       newNode.x=x;
       newNode.y=y;
       newNode.Orientation=currentNode.Orientation;
       
       newNode1.Name=structure.Nodes{4};
       newNode1.x=x+l*cos(theta);
       newNode1.y=y+l*sin(theta);
       newNode1.Orientation=currentNode.Orientation;
       
       newNode2.Name=structure.Nodes{3};
       newNode2.x=x+l*cos(theta)+(w+s)*sin(theta);
       newNode2.y=y-(w+s)*cos(theta)+l*sin(theta);
       newNode2.Orientation=currentNode.Orientation;
       
    case(3)
       %choose the reference x,y coordinates
       x=currentNode.x-(l*cos(theta)+(w+s)*sin(theta));
       y=currentNode.y-(-(w+s)*cos(theta)+l*sin(theta));
       
       %update the node, add other nodes to nodes list
       newNode.Name=structure.Nodes{1};
       newNode.x=x;
       newNode.y=y;
       newNode.Orientation=currentNode.Orientation;
       
       newNode1.Name=structure.Nodes{4};
       newNode1.x=x+l*cos(theta);
       newNode1.y=y+l*sin(theta);
       newNode1.Orientation=currentNode.Orientation;
       
       newNode2.Name=structure.Nodes{2};
       newNode2.x=x+(w+s)*sin(theta);
       newNode2.y=y-(w+s)*cos(theta);
       newNode2.Orientation=currentNode.Orientation;
      
    case(4)
       %choose the reference x,y coordinates
       x=currentNode.x-(l*cos(theta));
       y=currentNode.y-(l*sin(theta));
       
       %update the node, add other nodes to nodes list
       newNode.Name=structure.Nodes{1};
       newNode.x=x;
       newNode.y=y;
       newNode.Orientation=currentNode.Orientation;
       
       newNode1.Name=structure.Nodes{2};
       newNode1.x=x+(w+s)*sin(theta);
       newNode1.y=y-(w+s)*cos(theta);
       newNode1.Orientation=currentNode.Orientation;
       
       newNode2.Name=structure.Nodes{3};
       newNode2.x=x+l*cos(theta)+(w+s)*sin(theta);
       newNode2.y=y-(w+s)*cos(theta)+l*sin(theta);
       newNode2.Orientation=currentNode.Orientation;
       
end

%calculate coordinates of polygons
%polygon 1: upper line
poly1(1,1)=x-sin(theta)*(w/2);
poly1(1,2)=y+cos(theta)*(w/2);

poly1(2,1)=x+l*cos(theta)-sin(theta)*(w/2);
poly1(2,2)=y+l*sin(theta)+cos(theta)*(w/2);

poly1(3,1)=x+l*cos(theta)+sin(theta)*(w/2);
poly1(3,2)=y+l*sin(theta)-cos(theta)*(w/2);

poly1(4,1)=x+sin(theta)*(w/2);
poly1(4,2)=y-cos(theta)*(w/2);

%polygon 2: lower line
poly2(1,1)=x+sin(theta)*((w/2)+s);
poly2(1,2)=y-cos(theta)*((w/2)+s);

poly2(2,1)=x+l*cos(theta)+sin(theta)*((w/2)+s);
poly2(2,2)=y+l*sin(theta)-cos(theta)*((w/2)+s);

poly2(3,1)=x+l*cos(theta)+sin(theta)*((3*w/2)+s);
poly2(3,2)=y+l*sin(theta)-cos(theta)*((3*w/2)+s);

poly2(4,1)=x+sin(theta)*((3*w/2)+s);
poly2(4,2)=y-cos(theta)*((3*w/2)+s);

%update polygons array
polygons(end+1)=struct('Coord',poly1,'Layer',1);
polygons(end+1)=struct('Coord',poly2,'Layer',1);

%update nodes
nodes(j1)=newNode;
nodes(end+1)=newNode1;
nodes(end+1)=newNode2;


end

