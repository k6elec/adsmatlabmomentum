function [nodes,polygons]=createDGND(structure,polygons,nodes)
%createDGND.m adds the polygon that describes the groundplane (and the
%rectangular slots in it) to the polygon list. REMARK: This GND plane will
%be placed on layer 2.
%
%Needed fields are:
%   structure   This structure is the contains the information about the
%               ground plane that is created.
%               It has following fields:
%               structure.Parameters.Name: Name of the component.
%               structure.Parameters.L: Length of the GND plane.
%               structure.Parameters.W: Width of the GND plane.
%               structure.Parameters.Nodes: contains the nodes where
%               the ports are placed.
%               structure.Parameters.Xs: x-coordinate of the (first) slot (in mm/or m)
%               structure.Parameters.Ys: x-coordinate of the (first) slot (in mm/or m)
%               structure.Parameters.Ws: Width of the slot (in mm/or m)
%               structure.Parameters.Ls: Length of the slot (in mm/or m)
%               structure.Parameters.Ss: Spacing between slots (in mm/or m)
%               structure.Parameters.Ns: Number of slots (scalar)
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
%
%Remark: the function can only handle rectangular slots and they MAY NOT
%OVERLAP!
%TODO : -adapt code such that it can handle overlapping slots
%       -check whether the layout fits on the proposed GND plane
%       -now it is a symmetrical rectangle around (0,0) must be adapted!
%09/11/2015
%Matthias Caenepeel, Evi Van Nechel ELEC

% inputparser
p = inputParser();
p.addRequired('structure',@isstruct);
p.addRequired('polygons',@ismatrix);
p.addRequired('nodes',@isstruct);
p.StructExpand = true;
p.parse(structure,polygons,nodes);
args = p.Results();

%PATTERN = '\d*\.?\d+';
%This pattern is used to find the value and the unit of the parameters
%using the function regexp: ?<value> will fill in the field value with zero
%or more digits (*) a possible point \.? and one or more d, a space and
%than it fills in the field unit with zero or more word characters \w*
PATTERN2 = '(?<value>\d*\.?\d+)\s*(?<unit>\w*)';
N=length(args.polygons);

%Find the parameters from the structure GND
W=regexp(args.structure.Parameters.W,PATTERN2,'names');
w=str2double(W.value);

switch W.unit
    case 'mm'
        w=w*1e-3;
    case 'um'
        w=w*1e-6;
    case ''
    otherwise
        error('Unit W not known')
end

L=regexp(args.structure.Parameters.L,PATTERN2,'names');
l=str2double(L.value);

switch L.unit
    case 'mm'
        l=l*1e-3;
    case 'um'
        l=l*1e-6;
    case ''
    otherwise
        error('Unit L not known')
end


%Find the parameters from the structure slot struct
Ws=regexp(args.structure.Parameters.Ws,PATTERN2,'names');
w_slot=str2double(Ws.value);

switch Ws.unit
    case 'mm'
        w_slot=w_slot*1e-3;
    case 'um'
        w_slot=w_slot*1e-6;
    case ''
    otherwise
        error('Unit Ws not known')
end

Ls=regexp(args.structure.Parameters.Ls,PATTERN2,'names');
l_slot=str2double(Ls.value);

switch Ls.unit
    case 'mm'
        l_slot=l_slot*1e-3;
    case 'um'
        l_slot=l_slot*1e-6;
    case ''
    otherwise
        error('Unit Ls not known')
end

%check whether the slot is not to big and whether it is in the
%groundplane

Xs=regexp(args.structure.Parameters.Xs,PATTERN2,'names');
xs=str2double(Xs.value);
switch Xs.unit
    case 'mm'
        xs=xs*1e-3;
    case 'um'
        xs=xs*1e-6;
    case ''
    otherwise
        error('Unit Xs not known')
end


Ys=regexp(args.structure.Parameters.Ys,PATTERN2,'names');
ys=str2double(Ys.value);
switch Ys.unit
    case 'mm'
        ys=ys*1e-3;
    case 'um'
        ys=ys*1e-6;
    case ''
    otherwise
        error('Unit Ys not known')
end

Ss=regexp(args.structure.Parameters.Ss,PATTERN2,'names');
spacing=str2double(Ss.value);
switch Ss.unit
    case 'mm'
        spacing=spacing*1e-3;
    case 'um'
        spacing=spacing*1e-6;
    case ''
    otherwise
        error('Unit Ss not known')
end

if (xs<0)||(xs>l)||(abs(ys)>w/2)
    error('Begin coordinates of first slot not in ground plane.')
end

if (ys+(w_slot/2)>w/2)||(ys-(w_slot/2)<-w/2)||(xs+l_slot>l)
    error('Slot is too big.')
end

%Check if there is node in the nodes list that corresponds to a node of the MLIN2. If
%there is no node, there is something wrong!

nodeNotFound=1;
i1=1;

while (nodeNotFound)
    
    %find the index of the node in the nodes list that corresponds to a node
    %the MLIN2.
    j1=find(cellfun(@any,regexp({nodes.Name},['^' structure.Nodes{i1} '$'])));
    %choose the node corresponding to that index as the current node
    currentNode=nodes(j1);
    switch(length(currentNode))
        case(1)
            %if a node is found quit the while loop
            nodeNotFound=0;
            %find the index of the node of the MLIN
            nodeIndex=i1;
        case(0)
            if(i1==length(structure.Nodes))
                %no corresponding node in the nodes list
                %            disp('hallo')
                %            error('MOM:NNF','node not found');
                
                err = MException('MOM:NNF', 'node not found');
                throw(err);
                
            end
        otherwise
            error('multiple nodes found')
    end
    i1=i1+1;
end

% if exist('orientation','var')
%     currentNode.Orientation=currentNode.Orientation+orientation;
% else
%     if isfield(structure.Parameters,'orientation')
%         currentNode.Orientation=currentNode.Orientation+str2double(structure.Parameters.orientation);
%     end
% end

% theta=currentNode.Orientation*(pi/180);
% 
% 
% switch(nodeIndex)
%     case(1)
%         %choose the reference x,y coordinates
%         x=currentNode.x;
%         y=currentNode.y;
%         
%         if isempty(structure.Nodes{2}) == 0
%             %update the node, add other node to nodes list
%             newNode.Name=structure.Nodes{2};
%             newNode.x=x+l*cos(theta);
%             newNode.y=y+l*sin(theta);
%             newNode.Orientation=currentNode.Orientation;
%             newNode.Layer = currentNode.Layer;
%         end
%     case(2)
%         %choose the reference x,y coordinates
%         x=currentNode.x-cos(theta)*l;
%         y=currentNode.y-sin(theta)*l;
%         
%         if isempty(structure.Nodes{2}) == 0
%             %update the node, add other node to nodes list
%             newNode.Name=structure.Nodes{1};
%             newNode.x=x;
%             newNode.y=y;
%             newNode.Orientation=currentNode.Orientation;
%         end
% end

Ns=regexp(args.structure.Parameters.Ns,PATTERN2,'names');
NoSlots=str2double(Ns.value);
for n1=1:NoSlots
    %This is done such that the coordinates of the slots can be
    %processed such that it also works for slots that overlap.
    
    % slot coordinates
    sl{n1}.coord(1,1)=xs+(n1-1)*(l_slot+spacing);
    sl{n1}.coord(1,2)=ys+w_slot/2;
    
    sl{n1}.coord(2,1)=xs+l_slot+(n1-1)*(l_slot+spacing);
    sl{n1}.coord(2,2)=ys+w_slot/2;
    
    sl{n1}.coord(3,1)=xs+l_slot+(n1-1)*(l_slot+spacing);
    sl{n1}.coord(3,2)=ys-w_slot/2;
    
    sl{n1}.coord(4,1)=xs+(n1-1)*(l_slot+spacing);
    sl{n1}.coord(4,2)=ys-(w_slot/2);
    
end

Nsl=length(sl);

% groundplane coordinates
poly1(1,1)=0;
poly1(1,2)=-w/2;

poly1(2,1)=l;
poly1(2,2)=-w/2;

poly1(3,1)=l;
poly1(3,2)=(w/2);

n2=3;

for n3=1:Nsl
    % add slot coordinates to poly1
    poly1(n2+1,1)=sl{n3}.coord(1,1);
    poly1(n2+1,2)=w/2;
    
    poly1(n2+2:n2+5,:)=sl{n3}.coord(1:4,:);
    
    poly1(n2+6,1)=sl{n3}.coord(1,1);
    poly1(n2+6,2)=sl{n3}.coord(1,2);
    
    poly1(n2+7,1)=sl{n3}.coord(1,1);
    poly1(n2+7,2)=w/2;
    
    n2=n2+7;
    
end

% last coordinate ground plane
poly1(n2+1,1)=0;
poly1(n2+1,2)=(w/2);

% update polygons array
polygons(N+1)=struct('Coord',poly1,'Layer',2);

% %update nodes
% nodes(j1)=newNode;

end

