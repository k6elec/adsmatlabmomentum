function [polygons]=createGND(structure,polygons,varargin)
%createGND.m adds the polygon that describes the groundplan (and the slots
%in it) to the polygon list. REMARK: This GND plane will be placed on layer
%2.
%
%Needed fields are:
%   structure   This structure is the contains the information about the
%               ground plane that is created.
%               It has following fields:
%               structure.L: Length of the GND plane.
%               structure.W: Width (upper part) of the GND plane.
%               structure.nodes: contains the nodes where the ports are
%               placed.
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%Optional fields are:
%   slots       This struct array contains information about the position
%               and size of the rectangular slots. It contains following fields:
%               slots(i).x: x-position (in mm/or m)
%               slots(i).y: y-position (in mm/or m)
%               slots(i).L: length (in mm/or m)
%               slots(i).W: width(in mm/or m)
%
%Remark: the function can only handle rectangular slots and the MAY NOT
%OVERLAP!
%TODO : -adapt code such that it can handle overlapping slots
%       -check wheter the layout fits on the proposed GND plane
%       -now it is a symmetrical rectangle around (0,0) must be adapted!
%03/03/2014
%Matthias Caenepeel ELEC


p = inputParser();
p.addRequired('structure',@isstruct);
p.addRequired('polygons',@ismatrix);
p.addOptional('slots',[],@isstruct);
p.StructExpand = true;
p.parse(structure,polygons,varargin{:});
args = p.Results();




PATTERN2 = '(?<value>\d*\.?\d+)\s*(?<unit>\w*)';  
N=length(args.polygons);

%Find the parameters from the structure GND
W=regexp(args.structure.Parameters.W,PATTERN2,'names');
w=str2double(W.value);

switch W.unit
    case 'mm'
        w=w*1e-3;
    case ''
    otherwise
        error('Unit W not known')
end

L=regexp(args.structure.Parameters.L,PATTERN2,'names');
l=str2double(L.value);

switch L.unit
    case 'mm'
        l=l*1e-3;
    case ''
    otherwise
        error('Unit L not known')
end

if (isempty(args.slots))
    poly1(1,1)=0;
    poly1(1,2)=w/2;

    poly1(2,1)=l;
    poly1(2,2)=w/2;

    poly1(3,1)=l;
    poly1(3,2)=-(w/2);

    poly1(4,1)=0;
    poly1(4,2)=-(w/2);
else
    Ns=length(args.slots);
    
    for n1=1:Ns
        
        %Find the parameters from the structure slot struct
        W=regexp(args.slots(n1).W,PATTERN2,'names');
        w_slot=str2double(W.value);

        switch W.unit
            case 'mm'
                w_slot=w_slot*1e-3;
            case ''
            otherwise
                error('Unit W not known')
        end

        L=regexp(args.slots(n1).L,PATTERN2,'names');
        l_slot=str2double(L.value);

        switch L.unit
            case 'mm'
                l_slot=l_slot*1e-3;
            case ''
            otherwise
                error('Unit L not known')
        end
        
        %check wheter the slot is not to big and wheter it is in the
        %groundplane
        
        X=regexp(args.slots(n1).x,PATTERN2,'names');
        x=str2double(X.value);
        switch X.unit
            case 'mm'
                x=x*1e-3;
            case ''
            otherwise
                error('Unit X not known')
        end
       
        
        Y=regexp(args.slots(n1).y,PATTERN2,'names');
        y=str2double(Y.value);
        switch Y.unit
            case 'mm'
                y=y*1e-3;
            case ''
            otherwise
                error('Unit Y not known')
        end
        
        if (x<0)||(x>l)||(abs(y)>w/2)
            error(['Begin coordinates slot(' num2str(n1) ') not in ground plane.'])
        end
        
        if (y+(w_slot/2)>w/2)||(y-(w_slot/2)<-w/2)||(x+l_slot>l)
            error(['Slot(' num2str(n1) ') is too big.'])
        end
        
        %This is done such that the coordinates of the slots can be
        %processed such that it also works for slots that overlap.
        sl{n1}.coord(1,1)=x;
        sl{n1}.coord(1,2)=y+w_slot/2;
        
        sl{n1}.coord(2,1)=x+l_slot;
        sl{n1}.coord(2,2)=y+w_slot/2;
        
        sl{n1}.coord(3,1)=x+l_slot;
        sl{n1}.coord(3,2)=y-w_slot/2;
        
        sl{n1}.coord(4,1)=x;
        sl{n1}.coord(4,2)=y-(w_slot/2);
        
    end
    
    Nsl=length(sl);
    
    poly1(1,1)=0;
    poly1(1,2)=-w/2;

    poly1(2,1)=l;
    poly1(2,2)=-w/2;

    poly1(3,1)=l;
    poly1(3,2)=(w/2);
    
    n2=3;
    
    for n3=1:Nsl
        
        poly1(n2+1,1)=sl{n3}.coord(1,1);
        poly1(n2+1,2)=w/2;
        
        poly1(n2+2:n2+5,:)=sl{n3}.coord(1:4,:);
        
        poly1(n2+6,1)=sl{n3}.coord(1,1);
        poly1(n2+6,2)=sl{n3}.coord(1,2);
        
        poly1(n2+7,1)=sl{n3}.coord(1,1);
        poly1(n2+7,2)=w/2;
        
        n2=n2+7;
        
    end
    
    poly(n2+1,1)=0;
    poly1(n2+1,2)=(w/2);

end


polygons(N+1)=struct('Coord',poly1,'Layer',2);


end

