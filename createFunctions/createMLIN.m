function [nodes,polygons] = createMLIN(structure,polygons,nodes,orientation)
%createMLIN2.m adds the nodes to the array of nodes and also creates the polygon
%that corresponds to the MLIN2 structures and adds it to the polygon list.
%Needed fields are:
%   structure   This structure contains the information about the component
%               that is created. In this case a single microstrip line. It has
%               following fields:
%               structure.parameters.Name: Name of the component.
%               structure.parameters.L: Length of the line pair.
%               structure.parameters.W: Width of the line pair.
%               structure.parameters.Nodes: contains the nodes of the
%               components
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
% The struct arrays nodes and polygons are updated.
% Matthias Caenepeel, ELEC
% 18/02/2014 V1


% keyboard

[currentNode,nodeIndex] = LookforNodes(structure.Nodes,nodes);


%PATTERN = '\d*\.?\d+';
%This pattern is used to find the value and the unit of the parameters
%using the function regexp: ?<value> will fill in the field value with zero
%or more digits (*) a possible point \.? and one or more d, a space and
%than it fills in the field unit with zero or more word characters \w*
PATTERN2 = '(?<value>\d*\.?\d+)\s*(?<unit>\w*)';  

% extract the width of the microstrip line
W=regexp(structure.Parameters.W,PATTERN2,'names');
w=str2double(W.value);
% if there's a unit added to the end, take it into account
switch W.unit
    case 'mm'
        w=w*1e-3;
    case 'um'
        w=w*1e-6;
    case ''
    otherwise
        error('Unit not known')
end

% extract the length from the microstrip line
L=regexp(structure.Parameters.L,PATTERN2,'names');
l=str2double(L.value);
% if there is a dimension added, take that into account 
switch L.unit
    case 'mm'
        l=l*1e-3;
    case 'um'
        l=l*1e-6;
    case ''
    otherwise
        error('Unit not known')
end


% initialize the polygon coordinates (it is known that it will be a
% rectangle): thus 4 points with 2 coordinates (a,1)= x coordinate of point
% a, (a,2) = y coordinate of point a
poly=zeros(4,2);


if exist('orientation','var')
    currentNode.Orientation=currentNode.Orientation+orientation;
else
    if isfield(structure.Parameters,'orientation')
        currentNode.Orientation=currentNode.Orientation+str2double(structure.Parameters.orientation);
    end
end

% theta is the orientation in radians
theta=currentNode.Orientation*(pi/180);

switch(nodeIndex)
    case(1)
       %choose the reference x,y coordinates
       x=currentNode.x;
       y=currentNode.y;
      
       %update the node, add other node to nodes list
       newNode.Name=structure.Nodes{2};
       newNode.x=x+l*cos(theta);
       newNode.y=y+l*sin(theta);
       newNode.Orientation=currentNode.Orientation;
       
     case(2)
       %choose the reference x,y coordinates
       x=currentNode.x-cos(theta)*l;
       y=currentNode.y-sin(theta)*l;
       
       %update the node, add other node to nodes list
       newNode.Name=structure.Nodes{1};
       newNode.x=x;
       newNode.y=y;
       newNode.Orientation=currentNode.Orientation;
end

%calculate coordinates of polygon
poly(1,1)=x-sin(theta)*(w/2);
poly(1,2)=y+cos(theta)*(w/2);

poly(2,1)=x+l*cos(theta)-sin(theta)*(w/2);
poly(2,2)=y+l*sin(theta)+cos(theta)*(w/2);

poly(3,1)=x+l*cos(theta)+sin(theta)*(w/2);
poly(3,2)=y+l*sin(theta)-cos(theta)*(w/2);

poly(4,1)=x+sin(theta)*(w/2);
poly(4,2)=y-cos(theta)*(w/2);

%update polygons array
if isfield(structure.Parameters,'Layer')
    polygons(end+1)=struct('Coord',poly,'Layer',str2double(structure.Parameters.Layer));
else
    polygons(end+1)=struct('Coord',poly,'Layer',1);
end
%update nodes
nodes(end+1)=newNode;


end

