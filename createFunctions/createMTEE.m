function [nodes polygons] = createMTEE(structure,polygons,nodes,orientation,up)
%createMTEE.m adds the nodes to the array of nodes and also creates the polygon
%that corresponds to the MTEE (MTEE) structure and adds it to the polygon list.
%For now W1 must be the same as W2 (this must be still adapted).
%Needed fields are:
%   structure   This structure contains the information about the component
%               that is created. In this case a coupled line pair. It has
%               following fields:
%               structure.parameters.Name: Name of the component.
%               structure.parameters.W1: width 1.
%               structure.parameters.W2: width 2.
%               structure.parameters.W3: width 3.
%               structure.parameters.Nodes: contains the nodes of the
%               components
%
%   polygons    This array of structures contains the polygons that are
%               already created. It has following fields:
%               polygons(i).Coord: this contains the coordinates of the
%               polygon.
%               polygons(i).Layer: this contains the number of the layer.
%               The toplayer has number 1 or has no number.
%
%   nodes       This struct array contains information about the nodes and
%               has following fields:
%               nodes(i).Name: name of the node
%               nodes(i).x: x-coordinate of the node
%               nodes(i).y: y-coordinate of the node
%               nodes(i).Orientation: the orientation of the nodes
%               expressed in degrees.
%               nodes(i).Up: default this is 0. If it is 1, than the
%               component is upside down.
% The struct arrays nodes and polygons are updated.
%Matthias Caenepeel, ELEC
%27/05/2014 V1

%PATTERN = '\d*\.?\d+';
%This pattern is used to find the value and the unit of the parameters
%using the function regexp: ?<value> will fill in the field value with zero
%or more digits (*) a possible point \.? and one or more d, a space and
%than it fills in the field unit with zero or more word characters \w*

PATTERN2 = '(?<value>\d*\.?\d+)\s*(?<unit>\w*)';  

%Find the parameters from the structure MLIN2
W1=regexp(structure.Parameters.W1,PATTERN2,'names');
w1=str2double(W1.value);

switch W1.unit
    case 'mm'
        w1=w1*1e-3;
    case ''
    otherwise
        error('Unit W1 not known')
end

W2=regexp(structure.Parameters.W2,PATTERN2,'names');
w2=str2double(W2.value);

switch W2.unit
    case 'mm'
        w2=w2*1e-3;
    case ''
    otherwise
        error('Unit W2 not known')
end

W3=regexp(structure.Parameters.W3,PATTERN2,'names');
w3=str2double(W3.value);

switch W3.unit
    case 'mm'
        w3=w3*1e-3;
    case ''
    otherwise
        error('Unit W3 not known')
end

if (w1~=w2)
    error('W1 is not equal to W2')
end

%Check if there is node in the nodes list that corresponds to a node of the CLIN1. If
%there is no node, there is something wrong!

nodeNotFound=1;
i1=1;

while (nodeNotFound)
 
 %find the index of the node in the nodes list that corresponds to a node
 %of MTEE.
 j1=find(cellfun(@any,regexp({nodes.Name},['^' structure.Nodes{i1} '$'])));
 %choose the node corresponding to that index as the current node
 currentNode=nodes(j1);
 switch(length(currentNode))
     case(1)
         %if a node is found quit the while loop
         nodeNotFound=0;
         %find the index of the node of the MTEE
         nodeIndex=i1;
     case(0)
         if(i1==length(structure.Nodes))
           %no corresponding node in the nodes list
           err = MException('MOM:NNF', 'node not found');
           throw(err);
         end
     otherwise 
         error('multiple nodes found')
 end
 i1=i1+1;
end

%orientation is given in degrees, put it in radians

if exist('orientation','var')
    currentNode.Orientation=currentNode.Orientation+orientation;
end
theta=currentNode.Orientation*(pi/180);

if exist('up','var')
    currentNode.Up=up;
end


switch(nodeIndex)
    case(1)
       %choose the reference x,y coordinates
       x=currentNode.x;
       y=currentNode.y;
       
       %update the node, add other nodes to nodes list
       newNode.Name=structure.Nodes{2};
       newNode.x=x+w3*cos(theta);
       newNode.y=y+w3*sin(theta);
       newNode.Orientation=currentNode.Orientation;
       
       if isfield(currentNode,'Up')
           switch (currentNode.Up)
               case(0)
                   newNode1.Name=structure.Nodes{3};
                   newNode1.x=x+(w3/2)*cos(theta)+sin(theta)*(w1/2);
                   newNode1.y=y+(w3/2)*sin(theta)-cos(theta)*(w1/2);
                   newNode1.Orientation=currentNode.Orientation-90;
               case(1)
                   newNode1.Name=structure.Nodes{3};
                   newNode1.x=x+(w3/2)*cos(theta)-sin(theta)*(w1/2);
                   newNode1.y=y+(w3/2)*sin(theta)+cos(theta)*(w1/2);
                   newNode1.Orientation=currentNode.Orientation+90;
           end
       else
           newNode1.Name=structure.Nodes{3};
           newNode1.x=x+(w3/2)*cos(theta)+sin(theta)*(w1/2);
           newNode1.y=y+(w3/2)*sin(theta)-cos(theta)*(w1/2);
           newNode1.Orientation=currentNode.Orientation-90;
       end
       
      case(2)
       %choose the reference x,y coordinates
       x=currentNode.x-cos(theta)*w3;
       y=currentNode.y-sin(theta)*w3;
       
       %update the node, add other nodes to nodes list
       newNode.Name=structure.Nodes{1};
       newNode.x=x;
       newNode.y=y;
       newNode.Orientation=currentNode.Orientation;
       
       if isfield(currentNode,'Up')
           switch (currentNode.Up)
               case(0)
                   newNode1.Name=structure.Nodes{3};
                   newNode1.x=x+(w3/2)*cos(theta)+sin(theta)*(w1/2);
                   newNode1.y=y+(w3/2)*sin(theta)-cos(theta)*(w1/2);
                   newNode1.Orientation=currentNode.Orientation-90;
               case(1)
                   newNode1.Name=structure.Nodes{3};
                   newNode1.x=x+(w3/2)*cos(theta)-sin(theta)*(w1/2);
                   newNode1.y=y+(w3/2)*sin(theta)+cos(theta)*(w1/2);
                   newNode1.Orientation=currentNode.Orientation+90;
           end
       else
           newNode1.Name=structure.Nodes{3};
           newNode1.x=x+(w3/2)*cos(theta)+sin(theta)*(w1/2);
           newNode1.y=y+(w3/2)*sin(theta)-cos(theta)*(w1/2);
           newNode1.Orientation=currentNode.Orientation-90;
       end
       
       
    case(3)
       
       if isfield(currentNode,'Up')
           switch (currentNode.Up)
               case(0)
               %choose the reference x,y coordinates
               x=currentNode.x-((w3/2)*cos(theta)+sin(theta)*(w1/2));
               y=currentNode.y-((w3/2)*sin(theta)-cos(theta)*(w1/2));
               case(1)
               x=currentNode.x-((w3/2)*cos(theta)-sin(theta)*(w1/2));
               y=currentNode.y-((w3/2)*sin(theta)+cos(theta)*(w1/2));   
           end
       else
           %choose the reference x,y coordinates
           x=currentNode.x-((w3/2)*cos(theta)+sin(theta)*(w1/2));
           y=currentNode.y-((w3/2)*sin(theta)-cos(theta)*(w1/2));   
       end
       
           %update the node, add other nodes to nodes list
           newNode.Name=structure.Nodes{1};
           newNode.x=x;
           newNode.y=y;
           newNode.Orientation=currentNode.Orientation;

           newNode1.Name=structure.Nodes{2};
           newNode1.x=x+w3*cos(theta);
           newNode1.y=y+w3*sin(theta);
           newNode1.Orientation=currentNode.Orientation;
       
end

%calculate coordinates of polygons
%polygon 1: upper line
poly1(1,1)=x-sin(theta)*(w1/2);
poly1(1,2)=y+cos(theta)*(w1/2);

poly1(2,1)=x+w3*cos(theta)-sin(theta)*(w1/2);
poly1(2,2)=y+w3*sin(theta)+cos(theta)*(w1/2);

poly1(3,1)=x+w3*cos(theta)+sin(theta)*(w1/2);
poly1(3,2)=y+w3*sin(theta)-cos(theta)*(w1/2);

poly1(4,1)=x+sin(theta)*(w1/2);
poly1(4,2)=y-cos(theta)*(w1/2);

%update polygons array
polygons(end+1)=struct('Coord',poly1,'Layer',1);


%update nodes
nodes(j1)=newNode;
nodes(end+1)=newNode1;


end

